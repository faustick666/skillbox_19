#include <iostream>
#include <string>

using namespace std;


class Animal
{
public:
    Animal()
    {
        
    }

    virtual void Voice()
    {
        cout << "Animal spawn" << endl;
    }
};

class Dog : public Animal
{
public:
    
    Dog()
    {
        
    } 
    void Voice() override 
    {
        cout << "Whoof" << endl;
    }
   
};

class Cat : public Animal
{
public:
    
    Cat()
    {
        
    }
    void Voice() override
    {
        cout << "Meuw" << endl;
    }
};

class Duck : public Animal
{
public:
    
    Duck()
    {
        
    }
    void Voice() override
    {
        cout << "Khrya" << endl;
    }
};

int main()
{
    
    Animal* animal[3] = {new Dog,new Duck,new Cat};
    for (int i = 0; i < 3; i++)
    {
        animal[i]->Voice();
    }
    

    
    
    
  
    return 0;
}